

from collections import defaultdict



class Adjacency_list():
    """
        Representation of graph as a collection of unordered lists, one for each vertex.
    """
    def __init__(self):
        self.__edges = defaultdict(list)


    def __check_parameter_type(self, *v):
        """
        Checks if the parameter v is type int
        :param v:
        Parameter which type will be checked.
        """

        for x in v:
            assert type(x) is int, "vertex must be integer type"



    def neighbors(self, v):
        """
        Returns all v vertex neighbors.
        :param v:
        Vertex for which neighbors will be retrieved.
        :return:
        List of neighbors.
        """
        self.__check_parameter_type(v)
        return self.__edges[v]

    def delete(self, v1, v2):
        """
        Delete edge between v1 and v2.
        :param v1:
        First vertex.
        :param v2:
        Second vertex.
        """
        self.__check_parameter_type(v1, v2)
        if not self.adjacent(v1, v2):
            raise Exception("Edge between {0} and {1} not exists in the graph".format(v1,v2))

        l = self.__edges[v1]
        l.remove(v2)



    def add(self, v1, v2):
        """
        Add new vertex between v1 and v2
        :param v1:
        First vertex.
        :param v2:
        Second vertex
        """
        self.__check_parameter_type(v1, v2)
        if v2 not in self.__edges[v1]:
            self.__edges[v1].append(v2)
        else:
            raise Exception("Edge between {0} and {1} already exists in the graph".format(v1, v2))

    def adjacent(self, v1, v2):
        """
        Check if exist edge between two vertexes v1 and v2.
        :param v1:
        First vertex.
        :param v2:
        Second vertex.
        :return:
        True when edge exists, otherwise false.
        """
        self.__check_parameter_type(v1, v2)

        return v1 in self.__edges and v2 in self.__edges[v1]

    def __str__(self):
        result = ""

        for k, v in self.__edges.items():
            result += "{0}:{1}\n".format(str(k), ", ".join(map(str, v)))
        return result


