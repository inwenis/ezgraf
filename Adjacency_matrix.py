__author__ = 'Tomek'
# -*- coding: utf-8 -*-

# Klasa macierz sąsiedztwa
# Autor: Tomasz Jedynak
#
class Adjacency_matrix():

    def __init__(self,size):
        self.matrix = [[0 for i in range(size)] for i in range(size)]
        self.size = size

    # funkcja zwraca listę sąsiadów podanego wierzchołka
    # in: v - numer wierzchołka
    # out: result - numery wierzchołków sąsiadujących z 'v'
    def neighbors(self, v):
        assert v <= self.size , "Wierzchołek nie istnieje"
        result = list()
        for i in range(self.size):
            if self.matrix[v-1][i] == 1:
                result.append(i+1)
        return result

    # funkcja usuwa krawędź z grafu skierowanego
    # in: v1 - numer wierzchołka z którego wychodzi krawędź
    #     v2 - numer wierzchołka do którego dochodzi krawędź
    def delete(self, v1, v2):
        assert v1 <= self.size , "Pierwszy wierzchołek nie istnieje"
        assert v2 <= self.size , "Drugi wierzchołek nie istnieje"
        assert self.matrix[v1-1][v2-1]==1, "Krawędź nie istnieje"
        self.matrix[v1-1][v2-1]=0

    # funkcja tworzy krawędź grafu skierowanego
    # in: v1 - numer wierzchołka z którego wychodzi krawędź
    #     v2 - numer wierzchołka do którego dochodzi krawędź
    def add(self, v1, v2):
        assert v1 <= self.size , "Pierwszy wierzchołek nie istnieje"
        assert v2 <= self.size , "Drugi wierzchołek nie istnieje"
        assert self.matrix[v1-1][v2-1]==0, "Krawędź już istnieje"
        self.matrix[v1-1][v2-1]=1

    # funkcja usuwa wierzchołek grafu oraz wszystkie dochodzące i wychodzące z niego krawędzie
    # in: v - numer wierzchołka do usunięcia
    def deleteVertex(self, v):
        assert v <= self.size , "Wierzchołek nie istnieje"
        for i in range(self.size):
            self.matrix[i].pop(v-1)
        self.matrix.pop(v-1)
        self.size-=1

    # funkcja tworzy nowy wierzchołek grafu
    def addVertex(self):
        for i in range(self.size):
            self.matrix[i].append(0)
        self.matrix.append([])
        for i in range(self.size+1):
            self.matrix[self.size].append(0)
        self.size+=1

    # funkcja sprawdza czy istnieje krawędź z v1 do v2
    # in: v1 - numer wierzchołka z którego wychodzi krawędź
    #     v2 - numer wierzchołka do którego dochodzi krawędź
    # out: True jeśli są sąsiadami i False jeśli nie są
    def adjacent(self, v1, v2):
        assert v1 <= self.size , "Pierwszy wierzchołek nie istnieje"
        assert v2 <= self.size , "Drugi wierzchołek nie istnieje"
        return self.matrix[v1-1][v2-1] == 1

    # zwraca tekstową reprezentację grafu
    def __str__(self):
        result = ""
        for i in range(self.size):
            for j in range(self.size):
                result += str(self.matrix[i][j]) + " "
            result += "\n"
        return result